import React from "react";
import { Router, Route } from "react-router-dom";
import HomePage from "./HomePage";
import { createBrowserHistory as createHistory } from "history";
import "./App.css";
import TopBar from "./TopBar";
import FeedPage from "./FeedPage";
import News from "./News";
import NotFounds from "./NotFound";
import {Switch} from "react-router-dom";
import Footer from "./Footer";
import Cats from "./Cats/Cats";

const history = createHistory();

function App({ feedsStore }) {
  const a = 1;
  const b = 2;
  return (
    <div className="App">
      <Router history={history}>
          <TopBar />
          <Switch>
            <Route
              path="/"
              exact
              component={props => <HomePage {...props} feedsStore={feedsStore} />}
            />
            <Route
              path="/feed"
              exact
              component={props => <FeedPage {...props} feedsStore={feedsStore} />}
            />
            <Route
              path="/new"
              exact
              component={() => <News a = {a} b = {b} />}
            />
            <Route
              path="/cat"
              exact
              component={() => <Cats/>}
            />
            <Route
              path="*"
              component={() => <NotFounds />}
            />
          </Switch>
          <Footer />
      </Router>

    </div>
  );
}

export default App;
