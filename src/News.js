import React, { useState } from 'react';
import Button from "react-bootstrap/Button";
import 'rxjs';
import {Observable} from 'rxjs';

function News({a,b}) {
    // use of hook
    const [login,changeLogin] = useState("helo");
    const [arraySample,changeArray] = useState(['a','b','c']);
    // add to array
    function changeLoginState() {
        changeArray(arraySample => {
            return [...arraySample,'c'];
        });
    }
    function resetState() {
        changeArray(arraySample => {
            return [];
        });
    }
    function emitEvent() {
        console.log('event')
    }


    return(
        <div className="news" >
            News appear here {login}
            <button onClick={changeLoginState}>Change State</button>
            <button onClick={resetState}>Reset State</button>
            <button onClick={emitEvent}>New Promisse</button>
                {arraySample.map(item => (
                    <li>
                        {item}
                    </li>
                ))}
        </div>
    )
    
}

export default News;